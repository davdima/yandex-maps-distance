var q = require('q')
  , http = require('http')
  , https = require('https')
  ;

var YMap = function () {

  this.token = undefined;
//  this.url = undefined;
  this.userAgent = undefined;
  this.parseTokens = {
    token: /project_data\["token"\]="(.*?)"/i,
    route: /route:'(.*?)'/i
  };
};

YMap.prototype.getRequestOptions = function(uri, isSSL) {
  isSSL = isSSL === undefined ? false : isSSL;
  var options = {
    hostname: 'api-maps.yandex.ru',
    port: isSSL ? 443 : 80,
    path: uri,
    method: 'GET',
    headers: {
      'User-Agent': this.userAgent
    }
  };
  return options;
};

YMap.prototype.getYMAPData = function () {

  var def = q.defer();

  var request = http.request(this.getRequestOptions('/2.0-stable/?load=package.full&lang=ru-RU'), function (res) {

    var body = '';
    res.on('data', function (chank) {
      body += chank;
    });

    res.on('end', function () {
      def.resolve(body);
    });
  });
  request.on('error', function (e) {
    console.log(e);
  });
  request.end();

  return def.promise;
};


YMap.prototype.init = function (test) {
  var self = this;
  test = test === undefined ? false : test;
  var def = q.defer();
  this.userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/' + new Date().getMilliseconds() +' Firefox/2.0.0.13';
  this
    .getYMAPData()
    .then(function (body) {
      var tokenMatch = self.parseTokens.token.exec(body);
      if (tokenMatch.length > 0) {
        self.token = tokenMatch[1];
        if (test) self.token += '1';
        console.log('TOKEN: ' + self.token);

      }
//      var urlMatch = self.parseTokens.route.exec(body);
//      if (urlMatch) {
//        self.url = urlMatch[1].replace(/\\/g, "");
//        self.url = self.url.replace(/https:\/\//, '');
//        self.url = self.url.substring(0, self.url.length - 1);
//      }x
      def.resolve(self);
    })
  ;

  return def.promise;
};

YMap.prototype.buildRouteUrl = function (from, to, traffic) {
  var parts = [];
  parts.push(
    'callback=',
    'token=' + this.token,
    'lang=ru_RU',
    'rll=' + from + '~' + to,
    'sco=latlong'
  );

  if (traffic) parts.push('mode=jams');
  return '/services/route/1.2/route.xml?' + parts.join('&');
};

YMap.prototype.getInfo = function (from, to, index, traffic, callback) {

  var requestUrl = this.buildRouteUrl(from, to, traffic);
  var self = this;
  var request = https.request(this.getRequestOptions(requestUrl, true), function (res) {
    var data = '';
    res.on('data', function(chunk){
      data += chunk;
    });

    res.on('end', function(){
      callback(JSON.parse(data), index);
    });
  });
  request.on('error', function (e) {
    console.log(e);
  });
  request.end();
};

YMap.prototype.renewToken = function() {
  var def = q.defer();

  var result = {
    old: this.token,
    "new": undefined
  };

  var self = this;

  this.init().then(function(){
    result.new = self.token;
    def.resolve(result);
  });

  return def.promise;
};

var yMap = new YMap();


module.exports = yMap;