var restify = require('restify');
var ymap = require('./ymaps/distance').init(true);

var totalRequests = 1;

function respond(req, res, next) {
  var r = {
    success: true,
    data: req.params.name
  };
  res.send(r);
  next();
}

var server = restify.createServer();
server.use(restify.bodyParser());
server.use(restify.queryParser());
server.use(restify.acceptParser(server.acceptable));

server.pre(function (req, res, next) {
  req.headers.accept = 'application/json';  // screw you client!
  req.accepts('application/json');
  res.charSet('utf-8');
  return next();
});

server.get('renewToken', function (req, res, next) {
  ymap.then(function (map) {
    map.renewToken().then(function (response) {
      res.send(200, {
        token: response
      });

      return next();
    })
  })
});

server.get('totalRequests', function (req, res, next) {
  res.send(200, {
    totalRequests: totalRequests
  });

  return next();
});

server.get('/distance/', function (req, res, next) {
  var startTime = new Date().getTime();
  if (!req.params.hasOwnProperty('from') || req.params.from.length == 0) {
    res.status(400);
    return next(new restify.errors.InvalidArgumentError('Параметр "from" не должен быть пустым'));
  }

  if (!req.params.hasOwnProperty('to') || req.params.to.length == 0) {
    res.status(400);
    return next(new restify.errors.InvalidArgumentError('Параметр "to" не должен быть пустым'));
  }

  var responseObject = {
    success: true,
    data: {
      from: req.params.from,
      addresses: {}
    },
    totalRequests: totalRequests
  };

  var traffic = req.params.hasOwnProperty('traffic');
  var items = req.params.to, len = items.length, processedData = [];

  responseObject.traffic = traffic;

  ymap.then(function (map) {

    for (var i = 0; i < len; i += 1) {
      map
        .getInfo(req.params.from, items[i], i, traffic, function (routeInfo, index) {

          if (routeInfo.hasOwnProperty('error')) {
            return next(new restify.errors.NotAuthorizedError(routeInfo.error.message));
          }

          processedData[index] = {
            address: items[index],
            distance: routeInfo.response.properties.RouterRouteMetaData.Length.value || 0
          };
          totalRequests += 1;

        })
      ;
    }

    var waitInterval = setInterval(function () {
      if (processedData.length == len) {
        clearInterval(waitInterval);
        responseObject.data.addresses = processedData;
        var endTime = new Date().getTime();
        responseObject.executeTime = (endTime - startTime) / 1000;
        res.send(200, responseObject);
        return next();
      }
    }, 100);


  }).fail(function (err) {
    return next(new restify.errors.InvalidArgumentError(err));
  });
});

setInterval(function() {
  ymap.then(function (map) {
    map.renewToken().then(function (response) {
			console.log("renew done");
    })
  })
}, 10800000);

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});